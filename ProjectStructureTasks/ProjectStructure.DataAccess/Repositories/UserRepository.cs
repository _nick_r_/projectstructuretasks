﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.DataAccess.Interfaces.Repositories
{
    public class UserRepository : IRepository<User>
    {
        protected readonly DataContext context;

        public UserRepository(DataContext context)
        {
            this.context = context;
        }

        public async Task<User> Create(User item)
        {
            await context.Users.AddAsync(item);

            return item;
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            User user = await Get(id);
            if (user == null)
                throw new System.Exception("Invalid id");

            context.Users.Remove(user);
        }

        public async Task<User> Get(int id)
        {
            return await context.Users.FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IEnumerable<User>> GetList()
        {
            return await context.Users.ToListAsync();
        }

        public async Task<User> Update(int id, User item)
        {
            User user = await Get(id);
            if (user == null)
                throw new System.Exception("Invalid id");
            user.BirthDay = item.BirthDay;
            user.Email = item.Email;
            user.FirstName = item.FirstName;
            user.LastName = item.LastName;
            user.RegisteredAt = item.RegisteredAt;
            user.Team = item.Team;
            return user;
        }
    }
}
