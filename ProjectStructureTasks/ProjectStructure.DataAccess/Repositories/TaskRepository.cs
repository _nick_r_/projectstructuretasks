﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.DataAccess.Interfaces.Repositories
{
    public class TaskRepository : IRepository<Models.Task>
    {
        protected readonly DataContext context;

        public TaskRepository(DataContext context)
        {
            this.context = context;
        }

        public async Task<Models.Task> Create(Models.Task item)
        {
            await context.Tasks.AddAsync(item);
            return item;
        }

        public async Task Delete(int id)
        {
            Models.Task task = await Get(id);
            if (task == null)
                throw new System.Exception("Incorrect id");
            context.Tasks.Remove(task);
        }

        public async Task<Models.Task> Get(int id)
        {
            return await context.Tasks.FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IEnumerable<Models.Task>> GetList()
        {
            return await context.Tasks.ToListAsync();
        }

        public async Task<Models.Task> Update(int id, Models.Task item)
        {
            Models.Task task = await Get(id);
            if (task == null)
                throw new System.Exception("Incorrect id");
            task.Name = item.Name;
            task.Description = item.Description;
            task.Performer = item.Performer;
            task.State = item.State;
            task.FinishedAt = item.FinishedAt;
            return task;
        }
    }
}
