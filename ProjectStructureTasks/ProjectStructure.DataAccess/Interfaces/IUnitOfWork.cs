﻿using ProjectStructure.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<User> Users { get; }
        IRepository<Team> Teams { get; }
        IRepository<Project> Projects { get; }
        IRepository<Models.Task> Tasks { get; }

        void SaveChages();
        System.Threading.Tasks.Task SaveChangesAsync();
    }
}
