﻿using ProjectStructure.DataAccess.Models.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Models
{
    public class Team : BaseEntity
    {
        [MinLength(3), MaxLength(40)]
        public string Name { get; set; }
        public DateTime? CreatedAt { get; set; }

        public ICollection<User> Users { get; set; }
    }
}
