﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Models.Abstract
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
