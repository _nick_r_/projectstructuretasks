﻿using ProjectStructure.Shared.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Shared.DTO.Linq
{
    public class FourthTaskDTO
    {
        public int id;
        public string teamName;
        public IEnumerable<UserDTO> users;
    }
}
