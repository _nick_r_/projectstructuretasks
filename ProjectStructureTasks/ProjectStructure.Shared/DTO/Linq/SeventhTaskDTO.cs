﻿using ProjectStructure.Shared.DTO.Project;
using ProjectStructure.Shared.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Shared.DTO.Linq
{
    public class SeventhTaskDTO
    {
        public ProjectDTO project;
        public TaskDTO longestTask;
        public TaskDTO shortestTask;
        public int countUsers;
    }
}
