﻿using ProjectStructure.Shared.DTO.Linq;
using ProjectStructure.Shared.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IRequestService
    {
        Task<IDictionary<int, int>> FirstTask(int authorId);
        Task<IEnumerable<TaskDTO>> SecondTask(int performerId);
        Task<IEnumerable<ThirdTaskDTO>> ThirdTask(int performerId);
        Task<IEnumerable<FourthTaskDTO>> FourthTask();
        Task<IEnumerable<FifthTaskDTO>> FifthTask();
        Task<SixthTaskDTO> SixthTask(int userId);
        Task<SeventhTaskDTO> SeventhTask(int projectId);

    }
}
