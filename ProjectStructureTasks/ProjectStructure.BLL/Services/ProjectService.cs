﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Models;
using ProjectStructure.Shared.DTO.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<ProjectDTO> AddProject(ProjectDTO project)
        {
            Project projectModel = mapper.Map<ProjectDTO, Project>(project);
            Project result = await unitOfWork.Projects.Create(projectModel);
            await unitOfWork.SaveChangesAsync();
            return mapper.Map<Project, ProjectDTO>(result);
        }

        public async System.Threading.Tasks.Task DeleteProject(int id)
        {
            try
            {
                await unitOfWork.Projects.Delete(id);
                await unitOfWork.SaveChangesAsync();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<ProjectDTO>> GetAllProjects()
        {
            IEnumerable<Project> projects = await unitOfWork.Projects.GetList();
            return mapper.Map<IEnumerable<Project>, List<ProjectDTO>>(projects);
        }

        public async Task<ProjectDTO> GetProject(int id)
        {
            Project project = await unitOfWork.Projects.Get(id);
            return mapper.Map<Project, ProjectDTO>(project);
        }

        public async Task<ProjectDTO> UpdateProject(int id, ProjectDTO project)
        {
            try
            {
                Project projectModel = mapper.Map<ProjectDTO, Project>(project);
                Project result = await unitOfWork.Projects.Update(id, projectModel);
                await unitOfWork.SaveChangesAsync();
                return mapper.Map<Project, ProjectDTO>(result);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

    }
}
