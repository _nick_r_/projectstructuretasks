﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Shared.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Models;

namespace ProjectStructure.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<UserDTO> AddUser(UserDTO user)
        {
            User userModel = mapper.Map<UserDTO, User>(user);
            User result = await unitOfWork.Users.Create(userModel);
            await unitOfWork.SaveChangesAsync();
            return mapper.Map<User, UserDTO>(result);
        }

        public async System.Threading.Tasks.Task DeleteUser(int id)
        {
            try
            {
                await unitOfWork.Users.Delete(id);
                await unitOfWork.SaveChangesAsync();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<UserDTO>> GetAllUsers()
        {
            IEnumerable<User> users = await unitOfWork.Users.GetList();
            return mapper.Map<IEnumerable<User>, List<UserDTO>>(users);
        }

        public async Task<UserDTO> GetUser(int id)
        {
            User user = await unitOfWork.Users.Get(id);
            return mapper.Map<User, UserDTO>(user);
        }

        public async Task<UserDTO> UpdateUser(int id, UserDTO user)
        {
            try
            {
                User userModel = mapper.Map<UserDTO, User>(user);
                User result = await unitOfWork.Users.Update(id, userModel);
                await unitOfWork.SaveChangesAsync();
                return mapper.Map<User, UserDTO>(result);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
