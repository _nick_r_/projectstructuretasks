﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Models;
using ProjectStructure.Shared.DTO.Linq;
using ProjectStructure.Shared.DTO.Project;
using ProjectStructure.Shared.DTO.Task;
using ProjectStructure.Shared.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class RequestService : IRequestService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public RequestService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }


        public async Task<IDictionary<int, int>> FirstTask(int authorId)
        {
            var projects = await unitOfWork.Projects.GetList();
            var result = projects.Where(x => x.AuthorId == authorId)
                .Join(unitOfWork.Tasks.GetList().Result, p => p.Id, t => t.ProjectId, 
                (p, t) => new
            {
                projectId = p.Id,
                taskId = t.Id,
                taskName = t.Name,

            }).GroupBy(p => p.projectId).Select(g => new { projectId = g.Key, Count = g.Count() });
            return null;
        }

        public async Task<IEnumerable<TaskDTO>> SecondTask(int performerId)
        {
            var tasks = await unitOfWork.Tasks.GetList();
            var result = tasks.Where(x => (x.PerformerId == performerId) && (x.Name.Length < 45));
            return mapper.Map<IEnumerable<DataAccess.Models.Task>, IEnumerable<TaskDTO>>(result);
        }

        public async Task<IEnumerable<ThirdTaskDTO>> ThirdTask(int performerId)
        {
            var tasks = await unitOfWork.Tasks.GetList();
            var result =tasks.Where(x => (x.FinishedAt != null) && (x.FinishedAt.Value.Year == 2021) && (x.PerformerId == performerId)).Select(x => new
            {
                Id = x.Id,
                Name = x.Name
            });
            List<ThirdTaskDTO> realResult = new List<ThirdTaskDTO>();
            foreach (var r in result)
            {
                ThirdTaskDTO temp = new ThirdTaskDTO
                {
                    id = r.Id,
                    name = r.Name                  
                };
                realResult.Add(temp);
            }           
            return realResult;
        }

        public async Task<IEnumerable<FourthTaskDTO>> FourthTask()
        {
            var teams = await unitOfWork.Teams.GetList();
            var users = await unitOfWork.Users.GetList();
            var result = teams.GroupJoin(users,
                t => t.Id,
                u => u.TeamId,
                (team, user) => new
                {
                    TeamId = team.Id,
                    TeamName = team.Name,
                    Users = user.Where(x => x.BirthDay.Value.Year <= 2012)
                    .OrderByDescending(x => x.RegisteredAt).ToList()
                });
            List<FourthTaskDTO> realResult = new List<FourthTaskDTO>();
            foreach (var r in result)
            {
                IEnumerable<UserDTO> userDTOs = mapper.Map<IEnumerable<User>, IEnumerable<UserDTO>>(r.Users);

                FourthTaskDTO temp = new FourthTaskDTO
                {
                    id = r.TeamId,
                    teamName = r.TeamName,
                    users = userDTOs
                };
                realResult.Add(temp);
            }
            return realResult;

        }

        public async Task<IEnumerable<FifthTaskDTO>> FifthTask()
        {
            var users = await unitOfWork.Users.GetList();
            var tasks = await unitOfWork.Tasks.GetList();
            var result = users.OrderByDescending(x => x.FirstName)
                .GroupJoin(tasks,
                u => u.Id,
                t => t.PerformerId,
                (user, task) => new
                {
                    firstName = user.FirstName,
                    tasks = task.OrderByDescending(x => x.Name.Length).ToList()
                }
                );
            List<FifthTaskDTO> realResult = new List<FifthTaskDTO>();
            foreach (var r in result)
            {
                IEnumerable<TaskDTO> taskDTOs = mapper.Map<IEnumerable<DataAccess.Models.Task>, IEnumerable<TaskDTO>>(r.tasks);

                FifthTaskDTO temp = new FifthTaskDTO
                {
                    name = r.firstName,
                    tasks = taskDTOs
                };
                realResult.Add(temp);
            }
            return realResult;
        }

        public async Task<SixthTaskDTO> SixthTask(int userId)
        {
            var users = await unitOfWork.Users.GetList();
            var projects = await unitOfWork.Projects.GetList();
            var tasks = await unitOfWork.Tasks.GetList();

            var structure = users.Where(x => x.Id == userId).GroupJoin(projects, u => u.Id, p => p.AuthorId,
                (u, p) => new
                {
                    User = u,
                    Project = p.OrderBy(x => x.CreatedAt).Last(),
                }
                ).GroupJoin(tasks, x => x.User.Id, t => t.PerformerId,
                (u, t) => new
                {
                    User = u.User,
                    Project = u.Project,
                    AmountOfTasksForUser = t.Where(x => x.FinishedAt == null).Count()
                }
                ).GroupJoin(tasks, x => x.Project.Id, t => t.ProjectId,
                (u, t) => new
                {
                    User = u.User,
                    LastProject = u.Project,
                    AmountOfTasksForUser = u.AmountOfTasksForUser,
                    AmountOfTasksOnLastProject = t.Where(x => x.ProjectId == u.Project.Id).Count()
                }
                ).FirstOrDefault();

            SixthTaskDTO realResult = new SixthTaskDTO
            {
                user = mapper.Map<User, UserDTO>(structure.User),
                lastProject = mapper.Map<Project, ProjectDTO>(structure.LastProject),
                numberOfUnfinishedTasks = structure.AmountOfTasksForUser,
                numberOfTasksOnTheLast = structure.AmountOfTasksOnLastProject
            };
            return realResult;
        }

        public async Task<SeventhTaskDTO> SeventhTask(int projectId)
        {
            var projects = await unitOfWork.Projects.GetList();
            var tasks = await unitOfWork.Tasks.GetList();
            var users = await unitOfWork.Users.GetList();
            var teams = await unitOfWork.Teams.GetList();
            var structure = projects.Where(x => x.Id == projectId)
                .GroupJoin(tasks, p => p.Id, t => t.ProjectId,
                (p, t) => new
                {
                    Project = p,
                    LongTask = t.OrderByDescending(x => x.Description.Length).First(),
                    ShortTask = t.OrderBy(x => x.Name.Length).First(),
                    Quantity = teams.Where(x => x.Id == p.TeamId).GroupJoin(users, t => t.Id, u => u.TeamId,
                    (ts, us) => new
                    {
                        Team = ts,
                        Quantity = us.Count()
                    }
                    ).Select(x => x.Quantity).FirstOrDefault()
                }
                ).FirstOrDefault();

            SeventhTaskDTO realResult = new SeventhTaskDTO
            {
                project = mapper.Map<Project, ProjectDTO>(structure.Project),
                longestTask = mapper.Map<DataAccess.Models.Task, TaskDTO>(structure.LongTask),
                shortestTask = mapper.Map<DataAccess.Models.Task, TaskDTO>(structure.ShortTask),
                countUsers = structure.Quantity
            };
            return realResult;
        }
    }
}
