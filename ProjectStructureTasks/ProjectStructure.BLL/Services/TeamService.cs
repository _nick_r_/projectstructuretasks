﻿using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Models;
using ProjectStructure.Shared.DTO.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TeamService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<TeamDTO> AddTeam(TeamDTO team)
        {
            Team teamModel = mapper.Map<TeamDTO, Team>(team);
            Team result = await unitOfWork.Teams.Create(teamModel);
            await unitOfWork.SaveChangesAsync();
            return mapper.Map<Team, TeamDTO>(result);
        }

        public async System.Threading.Tasks.Task DeleteTeam(int id)
        {
            try
            {
                await unitOfWork.Teams.Delete(id);
                await unitOfWork.SaveChangesAsync();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<TeamDTO>> GetAllTeams()
        {
            IEnumerable<Team> teams = await unitOfWork.Teams.GetList();
            return mapper.Map<IEnumerable<Team>, List<TeamDTO>>(teams);
        }

        public async Task<TeamDTO> GetTeam(int id)
        {
            Team team = await unitOfWork.Teams.Get(id);
            return mapper.Map<Team, TeamDTO>(team);
        }

        public async Task<TeamDTO> UpdateTeam(int id, TeamDTO team)
        {
            try
            {
                Team teamModel = mapper.Map<TeamDTO, Team>(team);
                Team result = await unitOfWork.Teams.Update(id, teamModel);
                await unitOfWork.SaveChangesAsync();
                return mapper.Map<Team, TeamDTO>(result);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

    }
}
