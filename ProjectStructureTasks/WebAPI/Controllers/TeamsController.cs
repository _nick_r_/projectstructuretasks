﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.DataAccess.Models;
using ProjectStructure.Shared.DTO.Team;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.DataAccess.Interfaces.Repositories;
using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.BLL.Interfaces;

namespace ProjectStructure.WebAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService teamService;

        public TeamsController(ITeamService teamService)
        {
            this.teamService = teamService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<TeamDTO>>> GetTeams()
        {
            return Ok(await teamService.GetAllTeams());
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> CreateTeam([FromBody] TeamDTO newTeam)
        {
            return Ok(await teamService.AddTeam(newTeam));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<TeamDTO>> UpdateTeam(int id, [FromBody] TeamDTO newTeam)
        {
            return Ok(await teamService.UpdateTeam(id, newTeam));
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> DeleteTeam(int id)
        {
            await teamService.DeleteTeam(id);
            return NoContent();
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TeamDTO>> GetTeam(int id)
        {
            return Ok(await teamService.GetTeam(id));

        }
    }
}
