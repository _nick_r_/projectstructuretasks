﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Shared.DTO.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService projectService;

        public ProjectsController(IProjectService projectService)
        {
            this.projectService = projectService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<ProjectDTO>>> GetProjects()
        {
            return Ok(await projectService.GetAllProjects());
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> CreateProject([FromBody] ProjectDTO newProject)
        {
            return Ok(await projectService.AddProject(newProject));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<ProjectDTO>> UpdateProject(int id, [FromBody] ProjectDTO newProject)
        {
            return Ok(await projectService.UpdateProject(id, newProject));
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> DeleteProject(int id)
        {
            await projectService.DeleteProject(id);
            return NoContent();
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<ProjectDTO>> GetProject(int id)
        {
            return Ok(await projectService.GetProject(id));

        }
    }
}
