﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Shared.DTO.Linq;
using ProjectStructure.Shared.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RequestController : ControllerBase
    {
        private readonly IRequestService requestService;

        public RequestController(IRequestService requestService)
        {
            this.requestService = requestService;
        }

        [HttpGet("First/{id:int}")]
        public async Task<ActionResult<IDictionary<int, int>>> FirstRequest(int id)
        {
            return Ok(await requestService.FirstTask(id));
        }

        [HttpGet("Second/{id:int}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> SecondRequest(int id)
        {
            return Ok(await requestService.SecondTask(id));
        }

        [HttpGet("Third/{id:int}")]
        public async Task<ActionResult<IEnumerable<ThirdTaskDTO>>> ThirdRequest(int id)
        {
            return Ok(await requestService.ThirdTask(id));
        }

        [HttpGet("Fourth")]
        public async Task<ActionResult<IEnumerable<FourthTaskDTO>>> FourthRequest()
        {
            return Ok(await requestService.FourthTask());
        }

        [HttpGet("Fifth")]
        public async Task<ActionResult<IEnumerable<FifthTaskDTO>>> FifthRequest()
        {
            return Ok(await requestService.FifthTask());
        }

        [HttpGet("Sixth/{id:int}")]
        public async Task<ActionResult<SixthTaskDTO>> SixthRequest(int id)
        {
            return Ok(await requestService.SixthTask(id));
        }

        [HttpGet("Seventh/{id:int}")]
        public async Task<ActionResult<SixthTaskDTO>> SeventhRequest(int id)
        {
            return Ok(await requestService.SeventhTask(id));
        }
    }
}
