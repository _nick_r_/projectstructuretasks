﻿using AutoMapper;
using ProjectStructure.DataAccess.Models;
using ProjectStructure.Shared.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.WebAPI.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();


            CreateMap<UserCreateDTO, User>();
           
        }
    }
}
