﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.Shared.DTO.Project;
using ProjectStructure.DataAccess.Models;
using ProjectStructure.Shared.DTO.Task;

namespace ProjectStructure.WebAPI.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<DataAccess.Models.Task, TaskDTO>();
            CreateMap<TaskDTO, DataAccess.Models.Task>();


            CreateMap<TaskCreateDTO, DataAccess.Models.Task>();

        }
    }
}
